# Scripting

## Overview
This repository houses old/ancient scripts I have written years gone by for companies that
are generally no longer around.

Each of the scripts was written with a purpose.

## Scripts

* backup\_mysql.sh
  - One piece of database backup management.  Allowed for weekly and daily backups using percona's toolkit.
* filter\_urls.py
  - This was a prescreen take home coding challenge.  It was simply to demonstrate that I had some level
    of ability to write some code in python.  I'm not a developer, so I kept it 'simple'
* ipmi.sh
  - This little tool was used to plan out and help create a ton of ipmi mappings for our datacenter.  It was
    one of the very first times I had used an associative array in bash.  I keep it around as a reference to this day.
* libnagios.sh
  - Yeah, I know the name is wrong.  It's also not the actual final version that I would up writing. I lost that version.
    The point of this one (well, the one I wrote) was to make it so that when I was writing nagios scripts, I would just
    include the lib, and didn't need to keep on redefining things like exit statuses and codes for when a script returned.
    This was just before people started open sourcing various nagios toolkits.
* mapr\_disk.sh
  - written for the same company as ipmi.sh.  We had a decent sized mapr cluster (most of our nodes were jbods), and I got
    tired really quickly of having to create and manage the disk arrays so that they could be used by mapr.  I wrote this
    script and took advantage of how much simpler it made my life.  It's unfortunately only one part of the tools that I
    had written.
* mysql\_archive.sh
  - Written as the other piece of the backup\_mysql.sh toolkit.  Allowed us to archive the backups to S3.  Had somewhat
    reasonable checking to ensure it would work and be error resistant.
* namegen.py
  - Another take home exam.  This one was a bit more entertaining.  Was supposed to return a result set based on a parameter.
    Provide names based on 'first name',  'middle name', etc... which I allowed based on getopts.

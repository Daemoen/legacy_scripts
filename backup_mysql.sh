#!/bin/bash

###
# Name: backup_mysql.sh
# Purpose: Execute and manage mysql backups using Percona's XtraBackup facility
#
# Why not mysqldump:
# While mysqldump can be used and is often times used by less experienced admins,
# as database sizes get larger, mysqldump performance worsens, table locks become
# more and more unbearable, along with other detrimental affects.  Additionally,
# with mysqldump, there is no efficient way of handling incremental backups, which
# means any time you do a dump, you have to dump the *entire* dataset.
#
# By using xtrabackup, you are able to perform a single full backup, and then
# perform incremental backups using the offset value that is stored with the delta
# information.  This allows you to lessen the burden placed upon the slave machine
# you are performing the backup on.  It also makes it much easier to restore the
# database using xtrabackup, as you can point to the full, then to whichever
# incremental in the case that your db became corrupt at a given point and you need
# to perform 'database surgery'

# For the purposes of this script, we are assuming you will be doing a single full
# backup one day of the week (Sunday), and the other 6 days will be incremental backups.

# Makes it easier to display the script name using a variable for consumption in
# help message
SCRIPT_NAME="${0##*/}"

# Set the locations of the various directories
MYSQL_DIR="/var/lib/mysql"
BACKUP_DIR="/backups/mysql"
LOG_DIR="${BACKUP_DIR}/logs"
ARCHIVE_DIR="${BACKUP_DIR}/archive"
WEEKLY_DIR="Sunday"

# Set the DoW to full weekday name in a variable
DAY="$(date +%A)"

# Set the log/archive name date formats
DATE="$(date +%Y-%m-%d)"



# Checks the overall configuration to ensure script will operate as expected
# Ensures that directories and executable are present, if not tells you what
# needs to be done to resolve the issue(s).
check_config() {
        # Check for directories:  MYSQL_DIR, BACKUP_DIR, LOG_DIR
        echo -e "Beginning directory check"
        echo -e "========================="

        for DIR in ${MYSQL_DIR} ${BACKUP_DIR} ${LOG_DIR} ${ARCHIVE_DIR} ${WEEKLY_DIR}
                do
                        if [ -d ${DIR} ]; then
                                echo -e "Directory: ${DIR} exists."
                        else
                                echo -e "Directory: ${DIR} does not exist; exiting."
                                echo -e "======================="
                                echo -e "Aborted directory check"
                                echo -e ""
                                echo -e "Please create ${DIR} and execute the config check once more."
                                exit 1
                        fi
                done
        echo -e "========================="
        echo -e "Completed directory check"


        # Append a blank line to output for ease of reading sections
        echo -e ""


        # Check for xtrabackup
        echo -e "Beginning executable check"
        echo -e "=========================="

        if [ $(command -v xtrabackup) 2> /dev/null ]; then
                echo -e "Executable: 'xtrabackup' found in path."
        else
                echo -e "Executable: 'xtrabackup' not found in path; exiting."
                echo -e "========================"
                echo -e "Aborted executable check"
                echo -e ""
                echo -e "Please install xtrabackup and execute the config check once more."
                exit 1
        fi

        echo -e "=========================="
        echo -e "Completed executable check"
}



# Performs the daily incremental backup, writing the output to ${BACKUP_DIR}/${DAY}
# ie: /backups/mysql/Monday
# Logs to ${LOG_DIR}/backup_${DATE}.log
# ie: /backups/mysql/logs/2016-03-16.log
# Additionally, creates an archive of the data that has been created for upload to an
# offsite location
# ie: /backups/mysql/archive/2016-03-16.tgz
daily_backup() {
        xtrabackup --backup --target-dir="${BACKUP_DIR}/${DAY}" --incremental-basedir="${BACKUP_DIR}/${WEEKLY_DIR}" --datadir="${MYSQL_DIR}" &> ${LOG_DIR}/backup_${DATE}.log

        # Perform the actual archival process
        echo -e "Beginning archival of dataset" &> ${LOG_DIR}/backup_${DATE}.log
        tar -zcvf ${ARCHIVE_DIR}/${DATE}.tgz ${BACKUP_DIR}/${DAY}
        echo -e "Archive complete" &> ${LOG_DIR}/backup_${DATE}.log

        # Execute the mysql_archive.sh script
        bash mysql_archive.sh -u
}



# Displays the help message
help_manage() {
        echo -e "MySQL Incremental Backup Management Tool"
        echo -e ""
        echo -e "Options:"
        echo -e "${SCRIPT_NAME} -h            Print this helpfile"
        echo -e "${SCRIPT_NAME} -c            Perform config check (ensures script will work)"
        echo -e "${SCRIPT_NAME} -w            Perform weekly full backup"
        echo -e "${SCRIPT_NAME} -d            Perform daily incremental backup"
}



# Performs the weekly full backup, writing the output to ${BACKUP_DIR}/${WEEKLY_DIR}
# ie: /backups/mysql/Sunday
# Logs to ${LOG_DIR}/backup_${DATE}.log
# ie: /backups/mysql/logs/2016-03-16.log
# Additionally, creates an archive of the data that has been created for upload to an
# offsite location
# ie: /backups/mysql/archive/2016-03-16.tgz
weekly_backup() {
        xtrabackup --backup --target-dir="${BACKUP_DIR}/${WEEKLY_DIR}" --datadir="${MYSQL_DIR}" &> ${LOG_DIR}/backup_${DATE}.log

        # Perform the actual archival process
        echo -e "Beginning archival of dataset" &> ${LOG_DIR}/backup_${DATE}.log
        tar -zcvf ${ARCHIVE_DIR}/${DATE}.tgz ${BACKUP_DIR}/${WEEKLY_DIR}
        echo -e "Archive complete" &> ${LOG_DIR}/backup_${DATE}.log

        # Execute the mysql_archive.sh script
        bash mysql_archive.sh -u
}



# Allows us to pass in arguments on the command line
# Because we are using getopts, we cannot specify the long format options
# ie: --h will not work, nor will --help, or any argument option
# not specifically defined below.
while getopts ":cdhw" opt
        do
                case $opt in
                        h)
                                help_manage
                                ;;
                        c)
                                check_config
                                ;;
                        d)
                                daily_backup
                                ;;
                        w)
                                weekly_backup
                                ;;
                        \?)
                                echo "Invalid option: -$OPTARG" >&2
                                exit 1
                                ;;
                esac
        done

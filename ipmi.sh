#!/usr/local/bin/bash
# Bash KVM Descriptor
declare -A RACKS
RACKS=(
        ["1555-4305-01"]="1"
        ["1555-4305-02"]="43"
        ["1555-4305-03"]="85"
        ["1555-4305-04"]="127"
        ["1555-4305-05"]="169"
        ["1555-4305-06"]="211"
)

for RACK in "${!RACKS[@]}"
do
        IP="${RACKS[$RACK]}"
        for SLOT in {1..42}
        do
                echo "$RACK Slot: $SLOT Ip: 10.10.255.$IP";
                ((IP += 1))
        done
done

#!/bin/bash
# Set the disk partitions up using sfdisk
sfdisk --force /dev/sdb < /home/mmercer/sfdisk

# Declare the hashes that we are going to be using
declare -A partitions vg_system vg_mapr
partitions=(
        ["sdb1"]="vg_system"
        ["sdb2"]="vg_mapr"
)

vg_system=(
        ["home"]="25600"
        ["opt"]="12800"
        ["tmp"]="12799"
)

vg_mapr=(
        ["disk1"]="25599"
        ["disk2"]="25599"
        ["disk3"]="25599"
)


# Loop through the hash for partitions creating them
for part in "${!partitions[@]}"
do
        vg="${partitions[$part]}"
        pvcreate /dev/$part
        vgcreate $vg /dev/$part
done

# Loop through the system lvm array creating them with the respective logical extents
for syslv in "${!vg_system[@]}"
do
        sysle="${vg_system[$syslv]}"
        lvcreate -n $syslv -l +$sysle vg_system
        mkfs.ext4 /dev/vg_system/$syslv
        echo "/dev/vg_system/$syslv /$syslv ext4 defaults 1 2" >> /etc/fstab
        mkdir -p /tmp2/$syslv
        mount /dev/vg_system/$syslv /tmp2/$syslv
        rsync -av /$syslv/ /tmp2/$syslv
        rm -rf /$syslv/*
        umount /tmp2/$syslv
        mount /$syslv
        rm -rf /tmp2
done

# Loop through the mapr lvm array creating the partitions with the respective logical extents
for maprlv in "${!vg_mapr[@]}"
do
        maprle="${vg_mapr[$maprlv]}"
        lvcreate -n  $maprlv -l +$maprle vg_mapr
done​

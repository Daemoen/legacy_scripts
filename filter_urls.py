#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import re

# Write a function that processes an array of strings in the variable ‘url_list’ and checks if
#  they are URLs pointing to dom.com. A URL pointing to dom.com has the string “dom.com” as part of the host name. Note that the URLs in the list below are made up - the intent is not make a connection to these URLs.
#
#   url_list = [
# 		'http://sub-1.dom.com/p1?k1=v1&k2=v2#f1',
# 		'https://subc.dom.com/p2',
# 		'http://subd.dom.com/p3',
# 		'http://sub-1.dom1.com/p1?k1=v1&k2=v2#f1',
# 		'dom/com',
# 		'blah dom.com',
# 		'http://a dom com1']
#
#   The function has to return an array of a pair of the following form:
#
#   processed_list = [
# 		('http://sub-1.dom.com/p1?k1=v1&k2=v2f1',  “MATCH”),
# 		('blah dom.com', “NOT MATCH”),
# 		…]
#
#
#  Optionally, use Python re module:
# 	compiled_pattern = re.compile(r”some pattern”)
# 	match_object = compiled_pattern(string_var)    -- match_object not None => MATCH

url_list = [
		'http://sub-1.dom.com/p1?k1=v1&k2=v2#f1',
		'https://subc.dom.com/p2',
		'http://subd.dom.com/p3',
		'http://sub-1.dom1.com/p1?k1=v1&k2=v2#f1',
		'dom/com',
		'blah dom.com',
		'http://a dom com1' ]

# Since we just want a key value pair, this is far easier to work with as
# a dict
processed_list = {}
for url in url_list:

    # compiled_pattern = re.compile(ur"^https?://[\w-]+.dom.com/.*")
    # match_object = compiled_pattern(url)
    # This way is cleaner and more efficient ;  I had never used r"" so
    # had to play with the various escapes and flags.
    match_object = re.search(ur"^https?://[\w-]+.dom.com/.*", url)
    if match_object is not None:
        processed_list[url] = "MATCH"
    else:
        processed_list[url] = "NOT MATCH"

print processed_list

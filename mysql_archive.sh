#!/bin/bash

###
# Name: mysql_archive.sh
# Purpose: Execute and manage mysql archives on Amazon S3
#
# WARNING:  THIS SCRIPT WILL NOT EXECUTE ON OSX!!  ONLY ON LINUX!!!

# Makes it easier to display the script name using a variable for consumption in
# help message
SCRIPT_NAME="${0##*/}"

DATE="$(date +%Y-%m-%d)"
TODAY="$(date +"%b %d")"
LAST_SUNDAY="$(date -dlast-sunday +"%b %d")"
SUNDAY_DATE="$(date -dlast-sunday +"%Y-%m-%d")"
EOM="$(date -d"$(date +"%m")/01+1 month-1 day" +"%b %d")"
EOY="$(date -d"01/01+12 months-1 day" +"%b %d")"
DAY="$(date +"%A")"
DAILY="$(date +"%Y%m%d")"
MONTHLY="$(date +"%Y%m")"
YEARLY="$(date +"%Y")"

# Set this variable to the proper AWS values
PROFILE='ops-prod'
BUCKET='ops-prod-bucket'

# Set these variables to the proper directories
BACKUP_DIR="/backups/mysql"
LOG_DIR="${BACKUP_DIR}/logs"
ARCHIVE_DIR="${BACKUP_DIR}/archive"

# Cleanest way of testing aws s3 access is to wrap it in a function and grab the
# status code of the resultant ls; if we are successful, it will return 0 when we
# evaluate this later
test_aws() {
        aws --profile ${PROFILE} s3 ls s3://${BUCKET}
        status=$?
}

# Checks the overall configuration to ensure script will operate as expected
# Ensures that executables and credentials are present, if not tells you what
# needs to be done to resolve the issue(s).
check_config() {
        # Check for the 'aws' executable in path
        echo -e "Beginning executable check"
        echo -e "=========================="
        if [ $(command -v aws) 2> /dev/null ]; then
                echo -e "Executable 'aws' found in path."
        else
                echo -e "Executable 'aws' not found in PATH: ${PATH}"
                echo -e "========================"
                echo -e "Aborted executable check"
                echo -e ""
                echo -e "Please install the aws cli tools or add them to the path index,"
                echo -e "and run the config check once more."
                exit 1
        fi
        echo -e "=========================="
        echo -e "Completed executable check"

        # Append a blank line to output for ease of reading sections
        echo -e ""

        # Check for credentials in aws credential store
        echo -e "Beginning credential check"
        echo -e "=========================="
        if [ $(grep '${PROFILE}' ~/.aws/credentials) 2> /dev/null ]; then
                echo -e "Credential: ${PROFILE} found in credential store."
        else
                echo -e "Credential: ${PROFILE} not found in credential store; exiting"
                echo -e "========================"
                echo -e "Aborted credential check"
                echo -e ""
                echo -e "Please configure the ${PROFILE} credential in the aws credential store"
                echo -e "and run the config check once more. (default credential store is ~/.aws/credentials)"
                exit 1
        fi
        echo -e "=========================="
        echo -e "Completed credential check"

        # Append a blank line to output for ease of reading sections
        echo -e ""

        # Test aws and ensure S3 access
        # (This is different from simply testing to see if we have the command)
        echo -e "Beginning AWS S3 access check"
        echo -e "============================="
        # We don't need the output, so we discard it
        test_aws > /dev/null
        if [ "${status}" -eq 0 ]; then
                echo -e "Access: ${PROFILE} has access to AWS S3."
        else
                echo -e "Access: ${PROFILE} not able to access AWS S3; exiting"
                echo -e "==========================="
                echo -e "Aborted AWS S3 access check"
                echo -e ""
                echo -e "Please verify the ${PROFILE} credential in the aws credential store"
                echo -e "and run the config check once more. (default credential store is ~/.aws/credentials)"
                exit 1
        fi
        echo -e "============================="
        echo -e "Completed AWS S3 access check"
}



# Displays the help message
help_manage() {
        echo -e "MySQL S3 Archive Management Tool"
        echo -e ""
        echo -e "Options:"
        echo -e "${SCRIPT_NAME} -h            Print this helpfile"
        echo -e "${SCRIPT_NAME} -c            Perform config check (ensures script will work)"
        echo -e "${SCRIPT_NAME} -u            Perform the archive upload process"
}

upload_archive() {
        # We want 1 full week kept
        # Named: undertone_daily_20160801.tgz
        echo "Performing daily backup" &> ${LOG_DIR}/archive_${DATE}.log
        echo "Uploading: ${ARCHIVE_DIR}/${DATE}.tgz as undertone_daily_${DAILY}.tgz" &> ${LOG_DIR}/archive_${DATE}.log
        aws --profile ${PROFILE} s3 cp ${ARCHIVE_DIR}/${DATE}.tgz s3://${BUCKET}/undertone_daily_${DAILY}.tgz &> ${LOG_DIR}/archive_${DATE}.log
        echo "Upload complete." &> ${LOG_DIR}/archive_${DATE}.log

        # We want 1 monthly kept (last day of each month)
        # Named: undertone_mysql_monthly_201608.tgz (must be the most recent Sunday!)

        # Check to see if its the last day of the month
        if [ "${TODAY}" == "${EOM}" ]; then
                echo "Performing monthly backup" &> ${LOG_DIR}/archive_${DATE}.log
                echo "Most recent sunday: ${LAST_SUNDAY}" &> ${LOG_DIR}/archive_${DATE}.log
                echo "Uploading: ${ARCHIVE_DIR}/${SUNDAY_DATE}.tgz as undertone_monthly_${MONTHLY}.tgz" &> ${LOG_DIR}/archive_${DATE}.log
                aws --profile ${PROFILE} s3 cp ${ARCHIVE_DIR}/${SUNDAY_DATE}.tgz s3://${BUCKET}/undertone_monthly_${MONTHLY}.tgz &> ${LOG_DIR}/archive_${DATE}.log
                echo "Upload complete." &> ${LOG_DIR}/archive_${DATE}.log
        else
                echo "It is not the last day of the month, skipping monthly archive." &> ${LOG_DIR}/archive_${DATE}.log
        fi


        # We want 1 yearly kept (last day of the year)
        # Named: undertone_mysql_yearly_2016.tgz (must be the most recent Sunday!)

        # Check to see if its the last day of the year
        if [ "${TODAY}" == "${EOY}" ]; then
                echo "Performing yearly backup" &> ${LOG_DIR}/archive_${DATE}.log
                echo "Most recent sunday: ${LAST_SUNDAY}"
                echo "Uploading: ${ARCHIVE_DIR}/${SUNDAY_DATE}.tgz as undertone_yearly_${YEARLY}.tgz" &> ${LOG_DIR}/archive_${DATE}.log
                aws --profile ${PROFILE} s3 cp ${ARCHIVE_DIR}/${SUNDAY_DATE}.tgz s3://${BUCKET}/undertone_yearly_${YEARLY}.tgz &> ${LOG_DIR}/archive_${DATE}.log
                echo "Upload complete." &> ${LOG_DIR}/archive_${DATE}.log
        else
                echo "It is not the last day of the year, skipping yearly archive." &> ${LOG_DIR}/archive_${DATE}.log
        fi

}


# Allows us to pass in arguments on the command line
# Because we are using getopts, we cannot specify the long format options
# ie: --h will not work, nor will --help, or any argument option
# not specifically defined below.
while getopts ":chu" opt
        do
                case $opt in
                        h)
                                help_manage
                                ;;
                        c)
                                check_config
                                ;;
                        u)
                                upload_archive
                                ;;
                        \?)
                                echo "Invalid option: -$OPTARG" >&2
                                exit 1
                                ;;
                esac
        done

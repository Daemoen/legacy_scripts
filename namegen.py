#!/usr/bin/env python
# -*- coding:UTF-8 -*-
import getopt
import sys

def ngenhelp():
    print "namegen.py -afhlm"
    print "-a : Print full name matrix"
    print "-f : Print first names only"
    print "-h : Print this help dialogue"
    print "-l : Print last name only"
    print "-m : Print middle names only"


def matrix(names):
    first_or_middle_names = ['Stephanie', 'Jane']
    middle_names = ['Batman', 'Thor']
    last_name = "Larsen"

    if names == "firstormiddle":
        for name in first_or_middle_names:
            print name
    if names == "middle":
        for name in first_or_middle_names:
            print name
        for name in middle_names:
            print name
    if names == "last":
        print last_name
    if names == "matrix":
        for name in first_or_middle_names:
            print "%s %s %s" % (name, name, last_name)
            for middle in middle_names:
                print "%s %s %s" % (name, middle, last_name)

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "afhlm")
    except getopt.GetoptError:
        ngenhelp()

    for opt, arg in opts:
        if opt == "-a":
            names = "matrix"
        if opt == "-f":
            names = "firstormiddle"
        if opt == "-h":
            ngenhelp()
            sys.exit()
        if opt == "-l":
            names = "last"
        if opt == "-m":
            names = "middle"

        matrix(names)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        ngenhelp()

    main(sys.argv[1:])

__author__ = 'Marc Mercer'
#!/usr/bin/python2.7
import sys

status = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2, 'UNKNOWN': 3}

def summary(exit_code, exit_summary):
    print "%s: %s" % (exit_code, exit_summary)
    sys.exit(status[exit_code])
